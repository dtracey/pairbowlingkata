package com.summatech.PairBowlingKata;

public class ScoreBoardTemplate {
	public static final String NAME_FMT = "%6s|";
	public static final String ROLLS_FMT = "%3s%3s|";
	public static final String SCORE_FMT = "%6d|";
	public static final String FRAME_FMT = "%-6d|";

	public static final String FRAME_10_ROLLS_FMT = "%3s%3s%3s|%n";
	public static final String FRAME_10_SCORE_FMT = "%9d|%n";
	public static final String FRAME_10_FMT = "%-9s|%n";

	public static final char STRIKE_CHAR = 'X';
	public static final char SPARE_CHAR = '/';
	public static final char ZERO_CHAR = '-';

	public static void printTemplate(int numPlayers) {
		System.out.printf(NAME_FMT, "      ");

		for (int i = 1; i < 10; i++) {
			System.out.printf(FRAME_FMT, i);
		}

		System.out.printf(FRAME_10_FMT, 10);

		for (int i = 0; i < numPlayers; i++) {
			System.out.printf(NAME_FMT, i);

			for (int j = 1; j < 10; j++) {
				System.out.printf(ROLLS_FMT, 1, 2);
			}

			System.out.printf(FRAME_10_ROLLS_FMT + NAME_FMT, 1, 2, 3, "      ");

			for (int k = 1; k < 10; k++) {
				System.out.printf(SCORE_FMT, 3);
			}

			System.out.printf(FRAME_10_SCORE_FMT, 6);
		}
	}
}
