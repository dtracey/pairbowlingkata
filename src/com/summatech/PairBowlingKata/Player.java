package com.summatech.PairBowlingKata;

public class Player {
	private String name;
	private int[] rolls;
	private int[] scores;

	public Player(String name, int[] rolls) {
		this.name = name;
		this.rolls = rolls;
		scores = BowlingGame.calculateScores(rolls);
	}
}
